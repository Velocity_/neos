mkdir -p mnt
mount -o loop,offset=1048576 data/disk.img mnt
mkdir -p mnt/bin
cp data/kernel mnt/boot
cp data/grub.cfg mnt/boot/grub
sync
umount mnt
