ASMSOURCES=$(shell find src -type f -name "*.s")
ASMOBJECTS=$(addsuffix .o, $(basename $(ASMSOURCES)))

SOURCES=$(shell find src -type f -name "*.cpp")
OBJECTS=$(addsuffix .o, $(basename $(SOURCES)))

.PHONY: completely

completely: $(OBJECTS) $(ASMOBJECTS)
	i686-pc-elf-g++ -T data/linker.ld -o data/kernel -ffreestanding -O2 -nostdlib $(shell find bin -type f -name "*") -lgcc

.s.o:
	nasm -f elf -o bin/$(notdir $@) $<

.cpp.o:
	i686-pc-elf-g++ -Isrc -c $< -o bin/$(notdir $@) -std=c++11 -ffreestanding -O2 -Wall -Wno-write-strings -Wextra -fno-exceptions -fno-rtti
