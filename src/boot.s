[bits 32]
[extern boot_kernel]

ALIGN_ equ (1<<0)
MEMINFO equ (1<<1)
FLAGS equ (ALIGN_ | MEMINFO)

MAGIC equ 01BADB002h
CHECKSUM equ -(MAGIC + FLAGS)

section .multiboot

; Align on 4-byte boundaries
ALIGN 4
dd	MAGIC
dd	FLAGS
dd	CHECKSUM


section .bootstrap_stack
stack_bottom: ; Reserve 16Kb of stack data
	resb 16384

stack_top: ; Indicate that this is our stack top
	SECTION  .text

global _start
_start: ; Startup sequence is defined here
	mov  esp, stack_top
	call boot_kernel
	cli
hang: 
	hlt
	jmp hang


