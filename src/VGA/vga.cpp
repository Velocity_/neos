#pragma once

namespace NeOS {
namespace VGA {

/*
 * The colors used in the VGA
 */
#define VGA_COLOR_BLACK			0x0
#define VGA_COLOR_BLUE			0x1
#define VGA_COLOR_GREEN			0x2
#define VGA_COLOR_CYAN			0x3
#define VGA_COLOR_RED			0x4
#define VGA_COLOR_MAGENTA		0x5
#define VGA_COLOR_BROWN     	0x6
#define VGA_COLOR_LIGHTGREY 	0x7
#define VGA_COLOR_DARKGREY		0x8
#define VGA_COLOR_OCEANBLUE		0x9
#define VGA_COLOR_LIGHTGREEN	0xA
#define VGA_COLOR_LIGHTBLUE		0xB
#define VGA_COLOR_LIGHTRED		0xC
#define VGA_COLOR_PINK			0xD
#define VGA_COLOR_YELLOW		0xE
#define VGA_COLOR_WHITE			0xF

/*
 * The VGA memory region
 */
volatile char *vga = (volatile char *) 0xB8000;

int vgarow = 0;
int vgacol = 0;

/*
 * Fills the VGA screen with a solid background color
 */
void fillSolidBackground(int color) {
	for (int pos = 0; pos < (25 * 80 * 2); pos += 2) {
		vga[pos + 1] = color << 4;
	}
}

int strlen(char *str) {
	int i = 0;
	while (str[i] != 0x0)
		i++;
	return i;
}

void put(char c) {
	if (c == '\n') {
		vgarow++;
		vgacol = 0;
		return;
	}

	int pos = ((vgarow * 80) + vgacol) * 2;
	vga[pos] = c;
	vga[pos + 1] = 0;

	vgacol++;

	if (vgacol == 81) {
		vgacol = 0;
		vgarow++;
	}
}

void print(char *str) {
	int len = strlen(str);
	for (int i = 0; i < len; i++) {
		put(str[i]);
	}
}

void println(char *str) {
	int len = strlen(str);
	for (int i = 0; i < len; i++) {
		put(str[i]);
	}
	put('\n');
}

}
}
