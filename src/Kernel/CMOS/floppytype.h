#pragma once

namespace NeOS {
namespace Kernel {
namespace CMOS {

template<typename T> struct Tuple {
	T *val0;
	T *val1;
};

enum FloppyType {
	NO_DRIVE = 0,
	FLOPPY_360KB_5_25 = 1,
	FLOPPY_1_2MB_5_25 = 2,
	FLOPPY_720KB_3_5 = 3,
	FLOPPY_1_44MB_3_5 = 4,
	FLOPPY_2_88MB_3_5 = 5
};



}
}
}
