#pragma once

#include <Kernel/Ports/portio.h>
#include <Kernel/CMOS/floppytype.h>

namespace NeOS {
namespace Kernel {
namespace CMOS {

	Tuple<FloppyType> getFloppies(void);

}
}
}

