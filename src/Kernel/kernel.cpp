#include <VGA/vga.cpp>
#include <Kernel/Ports/portio.h>
#include <Kernel/CMOS/cmos.h>

namespace NeOS {
namespace Kernel {

using namespace NeOS;

static char pool[10000]; // 10kbytes of heap space yay :)
static int pool_pos = 0;

//extern "C" void __iowrite(int port, int val);
void print_iodebug(char *chars) {
	for (int i=0; i<VGA::strlen(chars); i++)
		Kernel::Ports::write8(0xE9, chars[i]);
}

void *malloc(int bytes) {
	if ((pool_pos + bytes) >= 10000)
		return 0;
	void *p = &pool[pool_pos];
	pool_pos += bytes;
	return p;
}

void ReadFromCMOS(unsigned char array[]) {
	unsigned char tvalue, index;

	for (index = 0; index < 128; index++) {
		__asm("cli;\n\tmovb %1, %%al;\n\tout %%al, $0x70;\n\tnop;\n\tnop;\n\tin $0x71, %%al;\n\tsti;\n\tmovb %%al, %0;" : "=r" (tvalue) : "r" (index) : "%eax");

		array[index] = tvalue;
	}
}

int modpow(int val, int power) {
	int res = val;
	for (int i = 0; i < power - 1; i++) {
		res = res * val;
	}

	return res;
}

char *strcat(char *part1, char *part2) {
	int len1 = VGA::strlen(part1);
	int len2 = VGA::strlen(part2);
	//char res[len1+len2+1];
	char *res = (char *) malloc(len1 + len2 + 1);
	for (int i = 0; i < len1; i++) {
		res[i] = part1[i];
	}
	for (int i = 0; i < len2; i++) {
		res[len1 + i] = part2[i];
	}
	res[len1 + len2] = 0x0; //Null terminator

	return res;
}

static char *reverse(char *str) {
	for (int i = 0, j = VGA::strlen(str) - 1; i < j; i++, j--) {
		char c = str[i];
		str[i] = str[j];
		str[j] = c;
	}
	return str;
}

static char *itoa(int signed_value, char *str, int base) {
	if (base < 1 || base > 36)
		return 0;

	int value;
	if (signed_value >= 0)
		value = signed_value;
	else
		value = -signed_value;

	int pos = 0;
	do {
		str[pos] = value % base + '0';
		if (str[pos] > '9')
			str[pos] += 7;
		pos++;
	} while ((value /= base) > 0);

	if (signed_value < 0)
		str[pos++] = '-';

	str[pos] = '\0';
	reverse(str);

	return str;
}

void integerToString(int value, char *resp) {
	int negative = 0;
	if (value < 0) {
		negative = 1;
		value = -value; //Remove sign extension, make positive
	}
	int digits = 1;
	while (420) {
		int testval = 1;
		for (int i = 0; i < digits; i++) {
			testval = testval * 10;
		}

		if (testval <= value) {
			digits = digits + 1;
		} else {
			break;
		}
	}

	//48 = 0
	int currentValue = value;
	int extraOffset = negative;
	for (int dig = digits; dig > 0; dig--) {
		int testval = 1;
		for (int i = 1; i < dig; i++) {
			testval = testval * 10;
		}

		int digval = currentValue / testval;
		currentValue = currentValue - (testval * digval);

		resp[extraOffset + (digits - (dig))] = 48 + digval;
	}

	if (negative) {
		resp[0] = '-';
	}

	resp[digits + extraOffset] = 0x0; //String terminator
}

int my_func(void);
char msg[128];

extern "C" void boot_kernel() {
	VGA::println("b.             8 8 8888888888       ,o888888o.       d888888o.   ");
	VGA::println("888o.          8 8 8888          . 8888     `88.   .`8888:' `88. ");
	VGA::println("Y88888o.       8 8 8888         ,8 8888       `8b  8.`8888.   Y8 ");
	VGA::println(".`Y888888o.    8 8 8888         88 8888        `8b `8.`8888.     ");
	VGA::println("8o. `Y888888o. 8 8 888888888888 88 8888         88  `8.`8888.    ");
	VGA::println("8`Y8o. `Y88888o8 8 8888         88 8888         88   `8.`8888.   ");
	VGA::println("8   `Y8o. `Y8888 8 8888         88 8888        ,8P    `8.`8888.  ");
	VGA::println("8      `Y8o. `Y8 8 8888         `8 8888       ,8P 8b   `8.`8888. ");
	VGA::println("8         `Y8o.` 8 8888          ` 8888     ,88'  `8b.  ;8.`8888 ");
	VGA::println("8            `Yo 8 888888888888     `8888888P'     `Y8888P ,88P' ");
	VGA::fillSolidBackground(0xF);
	print_iodebug("This is an IO debug test message.\n");
	
	Kernel::CMOS::getFloppies();

	integerToString(Kernel::Ports::read8(0x92), msg);
	print_iodebug(msg);
	print_iodebug(", ");
	integerToString(Kernel::Ports::read8(0x61), msg);
	print_iodebug(msg);
	//outb (0x70, (NMI_disable_bit << 7) | (selected CMOS register number)); 
	Kernel::Ports::write8(0x70, (0x80) | (0x10));
	print_iodebug("\nWaiting a bit... Value: ");
	integerToString(Kernel::Ports::read8(0x71) >> 4, msg);
	print_iodebug(msg);
	print_iodebug("\n");
	Kernel::Ports::write8(0x64, 0xEE);
	//Kernel::Ports::write8(0x60, 2);
}

}
}
