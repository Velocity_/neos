#include <Kernel/Ports/portio.h>

//using namespace NeOS::Kernel::Ports;

void NeOS::Kernel::Ports::write8(int port, int value) {
	__iowrite(port, value & 0xFF);
}

void NeOS::Kernel::Ports::write16(int port, int value) {
	__iowrite(port, value & 0xFF);
	__iowrite(port, (value >> 8) & 0xFF);
}

void NeOS::Kernel::Ports::write24(int port, int value) {
	__iowrite(port, value & 0xFF);
	__iowrite(port, (value >> 8) & 0xFF);
	__iowrite(port, (value >> 16) & 0xFF);
}

void NeOS::Kernel::Ports::write32(int port, int value) {
	__iowrite(port, value & 0xFF);
	__iowrite(port, (value >> 8) & 0xFF);
	__iowrite(port, (value >> 16) & 0xFF);
	__iowrite(port, (value >> 24) & 0xFF);
}

void NeOS::Kernel::Ports::write64(int port, long value) {
	write32(port, (int) value);
	write32(port, (int) (value >> 32L));
}

int NeOS::Kernel::Ports::read8(int port) {
	return __ioread(port);
}
