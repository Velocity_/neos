#pragma once

extern "C" void __iowrite(int port, int value);
extern "C" int __ioread(int port);

namespace NeOS {
	namespace Kernel {
		namespace Ports {

			//extern "C" void __iowrite(int port, int value);

			void write8(int port, int value);
			void write16(int port, int value);
			void write24(int port, int value);
			void write32(int port, int value);
			void write64(int port, long value);

			int read8(int port);

		}
	}
}
