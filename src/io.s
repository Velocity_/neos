[bits 32]
[global __iowrite]
[global __ioread]

__iowrite: ; void __iowrite(int port, int b)
	mov dx, [esp+4] ;0xE9
	mov al, [esp+8] ;65
	out dx, al
	ret

__ioread: ; int __ioread(int port)
	mov dx, [esp+4] ; port
	in al, dx ; read port 'dx' into al
	ret ; al contains value at this point
