mkdir -p mnt
sudo mount -o loop,offset=1048576 data/disk.img mnt
sudo mkdir -p mnt/bin
sudo cp data/kernel mnt/boot
sudo cp data/grub.cfg mnt/boot/grub
sync
sudo umount mnt
